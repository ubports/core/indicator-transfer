# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-03 15:52+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: an\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: /home/sunweaver/MyDocuments/4projects/ubports-upstream/lomiri-indicator-transfer.upstream/lomiri-indicator-transfer/po/../src//view-gmenu.cpp:444
msgid "Files"
msgstr ""

#: /home/sunweaver/MyDocuments/4projects/ubports-upstream/lomiri-indicator-transfer.upstream/lomiri-indicator-transfer/po/../src//view-gmenu.cpp:549
msgid "Successful Transfers"
msgstr ""

#: /home/sunweaver/MyDocuments/4projects/ubports-upstream/lomiri-indicator-transfer.upstream/lomiri-indicator-transfer/po/../src//view-gmenu.cpp:550
msgid "Clear all"
msgstr ""

#: /home/sunweaver/MyDocuments/4projects/ubports-upstream/lomiri-indicator-transfer.upstream/lomiri-indicator-transfer/po/../src//view-gmenu.cpp:556
msgid "Resume all"
msgstr ""

#: /home/sunweaver/MyDocuments/4projects/ubports-upstream/lomiri-indicator-transfer.upstream/lomiri-indicator-transfer/po/../src//view-gmenu.cpp:562
msgid "Pause all"
msgstr ""

#: /home/sunweaver/MyDocuments/4projects/ubports-upstream/lomiri-indicator-transfer.upstream/lomiri-indicator-transfer/po/../src//view-gmenu.cpp:627
#, c-format
msgid "Unknown Download (%s)"
msgstr ""
